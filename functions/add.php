<?php

require_once("Command.php");

class add extends Command{

    public function __construct(){
    }

    public function execute($param_array){
        return $param_array->params[0] + $param_array->params[1] . PHP_EOL;
    }
}

?>