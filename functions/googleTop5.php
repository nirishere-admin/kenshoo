<?php

require_once("Command.php");

class googleTop5 extends Command{

    private $api_key;
    private $cx_key;

    public function __construct(){
    }

    public function __destruct(){
        unset($this->api_key);
        unset($this->cx_key);
    }


    public function execute($param_array){
        
        $str = urlencode($param_array->params[0]);
        $api_key = $param_array->api_key; //"AIzaSyB9x5l3kjlXBWIIMK8ebF5sJftZuH1WK6I";
        $cx_key = $param_array->cx_key; //"006744657502699879201:safk4pz-d1m";

        $url = "https://www.googleapis.com/customsearch/v1?key={$api_key}&cx={$cx_key}&q={$str}";

        $body = file_get_contents($url);
        $json = json_decode($body);
        $result = "";
        $count = 0;

        if($json){

            if (isset($json->items) && $json->items){
            foreach ($json->items as $item){
                $count++;
                $result .= $count.'. '. $item->title. PHP_EOL;
                $result .= $item->link. PHP_EOL;
                
                if($count==5)
                    break;
                }
            }else{
                $result = "can't decode JSON items";
            }
        }else{
            $result = "can't decode JSON";
        }

        return $result . PHP_EOL;
    }
}

?>