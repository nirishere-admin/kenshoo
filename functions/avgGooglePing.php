<?php

require_once("Command.php");
require_once("Services.php");

class avgGooglePing extends Command{

    public function __construct(){
    }

    public function execute($param_array){
        $result = Services::getPingAvgTime($param_array->server_name);
        return $result . PHP_EOL;
    }
}

?>