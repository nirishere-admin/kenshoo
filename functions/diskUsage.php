<?php

require_once("Command.php");

class diskUsage extends Command{

    public function execute($params){
        
        $arr = array();
		$result = "";

		$r = exec("df -h", $arr);
		foreach($arr as $key => $value) {
			$result .= $value.PHP_EOL;
		}

		return "Total storage for HD :\n ".
		"======================" . PHP_EOL . $result . PHP_EOL;

    }
}

?>