<?php

require_once("Command.php");

class multiply extends Command{

    public function execute($param_array){
        $result =  $param_array->params[0] * $param_array->params[1] . PHP_EOL; 
        return $result;
    }
}

?>