<?php

class Services{
    public static function getPingAvgTime($dest){
        exec("ping -c 1 " . $dest . " | head -n 2 | tail -n 1 | awk '{print $7}'", $ping_time);
        return $ping_time[0] . PHP_EOL;
    }
}

?>