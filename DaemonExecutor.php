<?php 

require_once("Command.php");
require_once("NotificationsCenter.php");
require_once("DaemonInterface.php");

define("DAEMON_READ_COMMAND",0);
define("DAEMON_READ_PARAM",1);

define("FUNCTIONS_DIRECTORY", "functions/");

class DaemonExecutor extends DaemonInterface{
    
    private $config;
    private $command;
    private $nowReading;
    private $param_index;
    private $param_count;
    private $commandParams;
    
    protected function readConfigFile(){
        $json_cfg = file_get_contents("config.json");
        if($json_cfg !== false)
            $this->config = json_decode($json_cfg);
        else
            die("can't read configuration file");
    }

    public function __construct(){
        
        $this->nowReading = DAEMON_READ_COMMAND;
        $this->param_index = 0;
        $this->commandParams = null;
        $this->command = null;
        $this->readConfigFile();
    }

    protected function displayMenu(){
        
        $result = PHP_EOL;
        $result.= "Welcome to Telnet Daemon (hit 'Enter' to display this menu)". PHP_EOL;
        $result.= "===========================================================". PHP_EOL;

        $this->readConfigFile();
        foreach($this->config as $key=>$value){
            $result .= $key . ". " . $value->description . PHP_EOL;
        }
        return $result;
    }

    protected function fullExecute(){
        $result = $this->command->execute( $this->commandParams );
        if(isset($this->commandParams->callback))
            NotificationsCenter::notify($this, $this->commandParams->callback, $this->commandParams->params);
        unset($this->command);
        return "Output:" . PHP_EOL . $result 
        . $this->displayMenu();
    }

    public function receive($read){
        $this->sanitizeInput($read);
        
        switch($this->nowReading){
            case DAEMON_READ_COMMAND:
                if( $read == '' ) {
                    return $this->displayMenu();
                }
                $this->commandParams = $this->getCommandObjectForIdx( $read );
                if($this->commandParams === false){
                    return 'Command not found';
                }else{
                    $this->sanitizeCommmandObject($this->commandParams);
                    $commandLocator = FUNCTIONS_DIRECTORY . $this->commandParams->name . ".php";
                    require_once( $commandLocator );
                    $this->command = new $this->commandParams->name;
                    $temp_params = isset( $this->commandParams->params ) ? $this->commandParams->params : null;
                    $this->param_count = count( $temp_params );
                    $this->param_index = 0;
                    if($this->param_count){
                        $this->nowReading = DAEMON_READ_PARAM;
                        return $this->commandParams->params[$this->param_index] . PHP_EOL;
                    } else{ 
                        $result = $this->fullExecute();
                        return $result; // $this->responseObject($result, $this->commandParams->callback, $this->commandParams->params);
                    }
                }                

                break;

            case DAEMON_READ_PARAM:
                printf("reading param: %s" . PHP_EOL,$read);
                $this->commandParams->params[$this->param_index] = $read;
                $this->param_index++;
                if($this->param_index == $this->param_count){
                    $this->param_index = 0;
                    $this->nowReading = DAEMON_READ_COMMAND;
                    $result = $this->fullExecute() ;
                    return $result;
                }else
                    return $this->commandParams->params[$this->param_index] . PHP_EOL;
                break;

        }

    }

    public function getCommandObjectForIdx($idx){
        if(isset($this->config->$idx))
            return $this->config->$idx;
        else
            return false;
    }
    
    protected function sanitizeCommmandObject(&$obj){

        if(!isset($obj->callback))
            $obj->callback = null;
        if(!isset($obj->params))
            $obj->params = null;

    }

    protected function sanitizeInput(&$read){
        $read = trim($read);
        $read = preg_replace( '/[\<\>\`\{\}\'\*]/', '', $read );
    }

}

?>


