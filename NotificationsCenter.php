<?php 

class notificationsCenter{
 
    public static $subscribers = array();
    public static $broadcaster_objects = array();
    
    static public function subscribe($me, $them){
        $them_hash = spl_object_hash($them);
        if (array_key_exists($them_hash, self::$subscribers)){
            self::$subscribers[$them_hash][]=$me;
        }else{
            self::$subscribers[$them_hash]=array($me);
            self::$broadcaster_objects[$them_hash] = $them;
        }
    }
    
    static public function notify($broadcaster, $func, $params){
        $broadcaster_hash = spl_object_hash($broadcaster);
        if(array_key_exists($broadcaster_hash, self::$broadcaster_objects)){
            foreach(self::$subscribers[$broadcaster_hash] as $subscriber)
            $subscriber->$func($params);
        }
    }

} 


?>