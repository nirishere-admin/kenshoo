<?php

require_once("Daemon.php");
require_once("DaemonExecutor.php");
require_once("NotificationsCenter.php");

/**
 * Check dependencies
 */
if( ! extension_loaded('sockets' ) ) {
	echo "This example requires sockets extension (http://www.php.net/manual/en/sockets.installation.php)\n";
	exit(-1);
}

if( ! extension_loaded('pcntl' ) ) {
	echo "This example requires PCNTL extension (http://www.php.net/manual/en/pcntl.installation.php)\n";
	exit(-1);
}

if( !isset($argv[1]) || !is_numeric($argv[1]) ){
	echo "Socket missing. Usage: php daemon.php {port #}".PHP_EOL;
	exit(0);
}


$address = '0.0.0.0';
$port = $argv[1];

$daemon = new Daemon($address, $port);
$executor = new DaemonExecutor();
NotificationsCenter::subscribe($daemon, $executor);
$daemon->init( $executor );
$daemon->listen();

?>