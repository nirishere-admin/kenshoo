<?php

require_once("sock/SocketServer.php");

abstract class Observer{
    protected function update($func, $params){
    }
}

class Daemon extends Observer{

    public $server;
    public $commandInterface;
    
    private $address;
    private $port;
    private $client;

    public function __construct($_address, $_port){
        $this->address = $_address;
        $this->port = $_port;
    }

    public function init($commandInterface){
        $this->server = new \Sock\SocketServer($this->port, $this->address);
        $this->commandInterface = $commandInterface;
        $this->server->init();
    }


    public function listen(){
        $this->server->setConnectionHandler( array($this, 'onConnect') );
        $this->server->listen();
	}
	
    public function onConnect( $client ) {
        $pid = pcntl_fork();
        
        if ($pid == -1) {
             die('could not fork');
        } else if ($pid) {
            // parent process
            return;
        }
        
        $this->client = $client;
        
        $read = ''; // starting out
        printf( "[%s] Client connected on port %d\n", $this->client->getAddress(), $this->client->getPort() );
        
        while( true ) {
            
            $result = $this->commandInterface->receive( $read );
            $this->client->send( $result );
            $read = $this->client->read();
            if( $read == '' ) 
                break;
            if( $read === null ) {
                $this->disconnect();
                return false;
            }
            else {
                printf( "[%s] Received: %s", $this->client->getAddress(), $read );
            }
        }
	
		$this->disconnect();
    }

    public function disconnect(){
        
        $this->client->close();
        printf( "[%s] Disconnected\n", $this->client->getAddress() );
    }
    
}