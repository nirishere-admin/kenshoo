Kenshoo Excercise 
====================
- Development Ready Telnet Server ready for team
- A multithread PHP Daemon supporting multiple connections
- This server can have functionality enhanced without a restart, while online
- edit the config.json file to add and remove functionality 

Notes
---------------------
>1. a restart might be required for modifications on available commands

>2. To add functionality:  
	a. the corresponding command class file into the functions directory  
	b. the command descriptor into the config.json file


Requirements
---------------------
> sockets (http://www.php.net/manual/en/sockets.installation.php)

> pcntl (http://www.php.net/manual/en/pcntl.installation.php)

Installation
---------------------
> git clone https://nirishere-admin@bitbucket.org/nirishere-admin/kenshoo.git

Run
---------------------
>php telnetDaemon.php [port#]

>telnet 127.0.0.1 [port#]

Example
---------------------
>open terminal and type in:
>php telnetDaemon.php 1111

>open another terminal window and type in:
>telnet 127.0.0.1 1111

>play around!
